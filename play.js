let startSwith = true;

function startPapion() {
    const circle = document.getElementById('green-circle');
    const buttonText = document.getElementById('play-button');
    if (startSwith){
        circle.classList.add('animated');
        buttonText.innerHTML = 'Pause'
        startSwith = false;
        return;
    } else {
        startSwith = true;
        circle.classList.remove('animated');
        buttonText.innerHTML = 'Play'

    }

}
const playButton = document.getElementById('play-button').addEventListener('click', startPapion);